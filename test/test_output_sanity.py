import json
import os.path

from conftest import KTH_WL, thresholds


def SABjson_sanity_check(SABjson_file, arrival_threshold=None):
    """Perform some sanity checks on the given SABjson file. 
    If the SABjson was created with arrival delimitation approach 
    (arrival_threshold != None), checks also consistency of inter-arrival times.
    Consistency cannot be checked for max and last delimitation approaches as
    we lost the information about the original finish times."""

    with open(SABjson_file, "r") as fd:
        data = json.load(fd)

    assert "sessions" in data, "Missing required field 'sessions'"

    s_id = 1
    session_start_time = -1
    job_sub_time = -1

    for session in data["sessions"]:
        # Session sanity check
        assert int(session["id"]) == s_id, "Sessions should have consecutive ids"
        assert float(session["first_submit_time"]) >= session_start_time, "Sessions should be in increasing order of start time"
        session_start_time = float(session["first_submit_time"])

        assert len(session["preceding_sessions"]) == len(session["thinking_time_after_preceding_session"])

        # Job sanity check
        assert len(session["jobs"]) == int(session["nb_jobs"])
        assert float(session["jobs"][0]["subtime"]) == 0, "First job in a session should have a subtime of 0"

        for job in session["jobs"]:
            previous_job_subtime = job_sub_time
            job_sub_time = float(job["subtime"]) + session_start_time
            assert job_sub_time >= previous_job_subtime, "Jobs should be in increasing order of submission time"

            # Interrarrival consistency
            if arrival_threshold is not None and previous_job_subtime != -1:
                interrarrival = job_sub_time - previous_job_subtime
                if job == session["jobs"][0]: 
                    assert interrarrival >= arrival_threshold * 60, f"Session {s_id} was started but the interrarrival time was not suffisiant"
                else:
                    job_id = job["id"]
                    assert interrarrival < arrival_threshold * 60, f"Job {job_id} in session {s_id} should be in a new session"
        s_id += 1

    fd.close()

def SABjsons(out_dir):
    return [
        file for file in os.listdir(out_dir) if file[-8:] == ".SABjson"
    ]

def nb_users(out_dir):
    """Return the number of users for which a SABjson file has been created"""
    assert os.path.exists(out_dir)
    return len(SABjsons(out_dir))


def nb_jobs(out_dir):
    """Return the number of jobs in all the SABjsons in out_dir"""

    assert os.path.exists(out_dir)

    job_count = 0
    for file in SABjsons(out_dir):
        with open(f"{out_dir}/{file}", 'r') as fp:
            user_data = json.load(fp)
            for sess in user_data["sessions"]:
                job_count += int(sess["nb_jobs"])
    return job_count


def kth_sanity_check(out_dir):
    """Checks that the output files correspond to the input trace in features 
    like total number of jobs or number of users"""

    assert nb_users(out_dir) == KTH_WL.nb_users
    assert nb_jobs(out_dir) == KTH_WL.nb_jobs




def test_sanity_kth_last_SABjsons():
    for threshold in thresholds:
        out_dir = os.path.abspath(f"test-out/KTH_last_t{threshold}")

        kth_sanity_check(out_dir)

        for file in SABjsons(out_dir):
            SABjson_sanity_check(f"{out_dir}/{file}")


def test_sanity_kth_max_SABjsons():
    for threshold in thresholds:
        out_dir = os.path.abspath(f"test-out/KTH_max_t{threshold}")

        kth_sanity_check(out_dir)

        for file in SABjsons(out_dir):
            SABjson_sanity_check(f"{out_dir}/{file}")


def test_sanity_kth_arrival_SABjsons():
    for threshold in thresholds:
        out_dir = os.path.abspath(f"test-out/KTH_arrival_t{threshold}")

        kth_sanity_check(out_dir)

        for file in SABjsons(out_dir):
            SABjson_sanity_check(f"{out_dir}/{file}", arrival_threshold=threshold)
