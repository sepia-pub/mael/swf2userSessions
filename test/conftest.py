from cgi import test
import os, subprocess, sys

class KTH_WL:
    swf_file = "workloads/KTH-SP2-1996-2.1-cln.swf"
    nb_users = 214
    nb_jobs = 28475


thresholds = [0, 30, 1440]


def create_dir_rec_if_needed(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)


def run_script(delim, threshold,
              input_swf=KTH_WL.swf_file, test_name=None,
              dyn_red=True, graph=False, given_walltime_only=False):
    if test_name is None:
        if input_swf == KTH_WL.swf_file:
            pfx = "KTH"
        else:
            pfx = input_swf.split("/")[-1].split(".")[0]
        test_name = f"{pfx}_{delim}_t{threshold}"
        if not dyn_red:
            test_name += "_nodyn"

    out_dir = os.path.abspath(f'test-out/{test_name}')
    create_dir_rec_if_needed(out_dir)

    args = [sys.executable, 'swf2userSessions/swf2sessions.py', f'--{delim}', str(threshold), 
            '-q', input_swf, out_dir]
    if graph:
        args.append("--graph")
    if not dyn_red:
        args.append("--no_dynamic_reduction")
    if given_walltime_only:
        args.append('--given_walltime_only')
    
    cp = subprocess.run(args, check=True)


    return out_dir, cp