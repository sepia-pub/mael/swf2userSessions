"""Checks the validity of the script on instances that have been manually checked"""

from conftest import run_script
import pytest
import json, os

def load_expected_output(file_name):
    with open(os.path.abspath(f"test/expected_output/{file_name}.SABjson"), "r") as fd:
        return json.load(fd)

def load_output(file_name):
    with open(os.path.abspath(f"test-out/{file_name}/user1.SABjson"), "r") as fd:
        return json.load(fd)

def compare_with_expected_output(test_name):
    """Compare JSON field by field (better than a diff because 'description', 'date', 'version' fields could be different, as well as the format of some number)"""
    obtained = load_output(test_name)
    expected = load_expected_output(test_name)

    assert obtained["nb_res"] == expected["nb_res"]
    assert len(obtained["sessions"]) == len(expected["sessions"])

    for s_o, s_e in zip(obtained["sessions"], expected["sessions"]):
        assert int(s_e["id"]) == int(s_o["id"]), f"\
            In session {s_e['id']}"
        assert float(s_e["first_submit_time"]) == float(s_o["first_submit_time"]), f"\
            In session {s_e['id']}"
        assert s_e["preceding_sessions"] == s_o["preceding_sessions"], f"\
            In session {s_e['id']}"
        assert s_e["thinking_time_after_preceding_session"] == s_o["thinking_time_after_preceding_session"], f"\
            In session {s_e['id']}"
        assert int(s_e["nb_jobs"]) == int(s_o["nb_jobs"]), f"\
            In session {s_e['id']}"

        for j_o, j_e in zip(s_o["jobs"], s_e["jobs"]):
            assert int(  j_e["id"]) ==      int(j_o["id"]), f"\
                In session {s_e['id']}, job{j_e['id']}"
            assert float(j_e["profile"]) == float(j_o["profile"]), f"\
                In session {s_e['id']}, job{j_e['id']}"
            assert int(  j_e["res"]) ==     int(j_o["res"]), f"\
                In session {s_e['id']}, job{j_e['id']}"
            assert float(j_e["subtime"]) == float(j_o["subtime"]), f"\
                In session {s_e['id']}, job{j_e['id']}"
            assert float(j_e["walltime"]) == float(j_o["walltime"]), f"\
                In session {s_e['id']}, job{j_e['id']}"


def test_example_workload():
    """Simple tests with example workload"""

    test_name = "example_arrival_t60"
    run_script(delim="arrival", threshold=60, given_walltime_only=True,
        input_swf="workloads/example.swf")
    compare_with_expected_output(test_name)

    test_name = "example_arrival_t0"
    run_script(delim="arrival", threshold=0, given_walltime_only=True,
        input_swf="workloads/example.swf")
    compare_with_expected_output(test_name)

    test_name = "example_arrival_t0_nodyn"
    run_script(delim="arrival", threshold=0, given_walltime_only=True,
        input_swf="workloads/example.swf", dyn_red=False)
    compare_with_expected_output(test_name)

    test_name = "example_max_t0"
    run_script(delim="max", threshold=0, given_walltime_only=True,
        input_swf="workloads/example.swf")
    compare_with_expected_output(test_name)

    test_name = "example_last_t30"
    run_script(delim="last", threshold=30, given_walltime_only=True,
        input_swf="workloads/example.swf")
    compare_with_expected_output(test_name)

def test_edge_cases():
    """Hand written SWF and SABjson files, testing some edge cases.
    Same tests than in batmen."""

    test_name = "load_platform"
    run_script(delim="arrival", threshold=10, given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    compare_with_expected_output(test_name)

    test_name = "many_following"
    run_script(delim="arrival", threshold=0,  given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    compare_with_expected_output(test_name)

    test_name = "many_preceding"
    run_script(delim="arrival", threshold=0,  given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    compare_with_expected_output(test_name)

    test_name = "no_session"
    run_script(delim="max", threshold=42,  given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    with pytest.raises(FileNotFoundError):
        load_expected_output(test_name)

    test_name = "one_session_many_jobs"
    run_script(delim="last", threshold=10,  given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    compare_with_expected_output(test_name)

    test_name = "zero_think_time"
    run_script(delim="last", threshold=0,  given_walltime_only=True,
               input_swf=f"test/workload/{test_name}.swf", test_name=test_name)
    compare_with_expected_output(test_name)