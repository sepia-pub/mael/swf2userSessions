import sys

from conftest import run_script, KTH_WL, thresholds

import os, subprocess
import pytest
import json


def test_bad_input():
    with pytest.raises(subprocess.CalledProcessError):
        run_script('arrival', 30, input_swf="grumpf")


def test_kth_delim_last():
    """Launch swf2userSessions with 'last' delimitation approach and several threshold values"""

    for threshold in thresholds:
        out_dir, _ = run_script('last', threshold)

        assert os.path.exists(out_dir)
        assert len(os.listdir(out_dir)) > 0


def test_kth_delim_arrival():
    """Launch swf2userSessions with 'arrival' delimitation approach and several threshold values"""

    for threshold in thresholds:
        out_dir, _ = run_script('arrival', threshold)

        assert os.path.exists(out_dir)
        assert len(os.listdir(out_dir)) > 0


def test_kth_delim_max():
    """Launch swf2userSessions with 'max' delimitation approach and several threshold values"""

    for threshold in thresholds:
        out_dir, _ = run_script('max', threshold)

        assert os.path.exists(out_dir)
        assert len(os.listdir(out_dir)) > 0


def test_graph():
    """Launch a couple of instances with the --graph option and checks if the graphs are output"""

    instances = ["", "", ""]
    instances[0], _ = run_script('arrival', 0, graph=True)
    instances[1], _ = run_script('last', 30, graph=True)
    instances[2], _ = run_script('max', 1440, graph=True)

    for out_dir in instances:
        assert os.path.exists(out_dir)
        assert os.path.exists(f"{out_dir}/graphs")
        assert len(os.listdir(f"{out_dir}/graphs")) == KTH_WL.nb_users


