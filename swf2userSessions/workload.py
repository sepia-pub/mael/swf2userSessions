#!/usr/bin/env python3

"""SWF types and functions."""

from enum import Enum, unique
from math import ceil


@unique
class SwfField(Enum):
    """Maps SWF columns and their meaning."""

    JOB_ID = 1
    SUBMIT_TIME = 2
    WAIT_TIME = 3
    RUN_TIME = 4
    ALLOCATED_PROCESSOR_COUNT = 5
    AVERAGE_CPU_TIME_USED = 6
    USED_MEMORY = 7
    REQUESTED_NUMBER_OF_PROCESSORS = 8
    REQUESTED_TIME = 9
    REQUESTED_MEMORY = 10
    STATUS = 11
    USER_ID = 12
    GROUP_ID = 13
    APPLICATION_ID = 14
    QUEUD_ID = 15
    PARTITION_ID = 16
    PRECEDING_JOB_ID = 17
    THINK_TIME_FROM_PRECEDING_JOB = 18


class Job:
    """Class representing a job in the workload."""

    def __init__(self,
                 job_id:int,
                 submit_time:float,
                 finish_time:float,
                 start_time:float=None,
                 nb_requested_resources:int=None,
                 walltime:float=None,
                 job_grain:int=1):
        self.id = job_id
        self.submit_time = submit_time
        self.start_time = start_time
        self.finish_time = finish_time
        self.res = nb_requested_resources
        self.walltime = walltime
        self.jg = job_grain

    def rounded_up_profile(self):
        """Rounded up profile, to the next job grain"""
        run_time = self.finish_time - self.start_time
        return int(ceil(run_time / self.jg) * self.jg)

    def to_dict(self, session_start_offset=0):
        """Job object to dictionnary, for printing or json export."""
        return {
            "id": self.id,
            "profile": str(self.rounded_up_profile()),
            "res": self.res,
            "subtime": self.submit_time - session_start_offset,
            "walltime": self.walltime
        }



class Session:
    """Class representing a user session."""

    def __init__(self, id, first_submit):
        self.id = id
        self.first_submit = first_submit
        self.preceding_sessions = []
        self.tt_after_prec_sess = []
        self.jobs = [] # list of Job objects

        self.max_nb_res = 0
        self.max_finish_time = 0

    def add_job(self, job):
        """Add job to the session"""
        self.jobs.append(job)
        self.max_finish_time = max(job.finish_time, self.max_finish_time)
            

    def to_dict(self):
        """Session object to dictionnary, for printing or json export."""

        return {
            "id": self.id,
            "first_submit_time": self.first_submit,
            "preceding_sessions": self.preceding_sessions,
            "thinking_time_after_preceding_session": self.tt_after_prec_sess,
            "nb_jobs": len(self.jobs),
            "jobs": [j.to_dict(session_start_offset = self.first_submit)
                        for j in self.jobs]
        }


    def to_session_stat(self):
        """Return a dictionnary with session stats"""

        return {
            "first_submit_time": self.first_submit,
            "last_submit_time": max([j.submit_time for j in self.jobs]),
            "finish_time": self.max_finish_time,
            "nb_jobs": len(self.jobs)
        }