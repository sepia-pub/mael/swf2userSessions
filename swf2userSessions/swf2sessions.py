#!/usr/bin/env python3

import argparse, json, re, os, warnings
from swf2userSessions.user_session_builder import User
from swf2userSessions.workload import SwfField, Job


def swf2sessions(input_swf, out_dir, delim_approach, delim_threshold,
                 dynamic_reduction, build_graph_rep, session_stat, no_SABjson_output,
                 job_walltime_factor, given_walltime_only, job_grain, quiet):
    users = {}

    # Read SWF
    element = '([-+]?\d+(?:\.\d+)?)'
    r = re.compile('\s*' + (element + '\s+') * 17 + element + '\s*')
    i = 0
    nb_jobs, not_valid = 0, 0
    for line in input_swf:
        i += 1
        if not quiet and i % 100000 == 0:
            print(f"\r\033[KProcessing swf line {i}", end="")

        res = r.match(line)
        if res:
            nb_jobs += 1

            # Retreive values
            job_id = int(res.group((SwfField.JOB_ID.value)))
            submit_time = max(0, float(res.group(SwfField.SUBMIT_TIME.value)))
            wait_time = float(res.group(SwfField.WAIT_TIME.value))
            run_time = float(res.group(SwfField.RUN_TIME.value))
            user_id = str(res.group(SwfField.USER_ID.value))

            if given_walltime_only:
                walltime = float(res.group(SwfField.REQUESTED_TIME.value))
            else:
                walltime = max(job_walltime_factor * run_time,
                               float(res.group(SwfField.REQUESTED_TIME.value)))
            nb_res = int(res.group(SwfField.ALLOCATED_PROCESSOR_COUNT.value))

            start_time = submit_time + wait_time
            finish_time = submit_time + wait_time + run_time

            job = Job(job_id, submit_time, finish_time, start_time, nb_res,
                      walltime, job_grain=job_grain)

            if check_sanity(job):
                if user_id not in users:
                    users[user_id] = User(user_id,
                                        delim_approach,
                                        delim_threshold,
                                        dynamic_reduction=dynamic_reduction,
                                        build_graph_rep=build_graph_rep)
                user = users[user_id]
                user.add_job(job)
            
            else:
                not_valid += 1

    # SWF finished, write output files
    #   - SABjson:
    if not(no_SABjson_output):
        if not(os.path.exists(out_dir)):
            os.makedirs(out_dir)
        
        for user_id, user in users.items():
            with open(f"{out_dir}/user{user_id}.SABjson", "w") as file:
                json.dump(user.to_dict(), file)

    #   - graph:
    if build_graph_rep:
        if not(os.path.exists(f"{out_dir}/graphs")):
            os.makedirs(f"{out_dir}/graphs")        

        for _, user in users.items():
            user.export_dependancy_graph(f"{out_dir}/graphs")

    #   - session stat:
    if session_stat:
        stats = {}
        for user_id, user in users.items():
            stats[user_id] = user.to_session_stat()

        with open(f"{out_dir}_session_stat.json", "w") as file:
            json.dump(stats, file)

    if not quiet:
        print("\nSWF parsing done.")
        print(f"{nb_jobs} jobs parsed, {not_valid} jobs were filtered out as they did not pass the sanity checks")
        print("Number of users:    ", len(users))
        print("Number of sessions: ",
              sum([len(u.sessions) for u in users.values()]))
        print(f"The output files have been stored in the folder {out_dir}")


def check_sanity(job:Job):
    """Check basic properties that a recorded jobs should have"""

    if job.res <= 0:
        warnings.warn(f"Invalid job: job {job.id} has ALLOCATED_PROCESSOR_COUNT <= 0")
        return False
    if not( job.submit_time <= job.start_time <= job.finish_time ):
        warnings.warn(f"Invalid job: job {job.id} don't have submit <= start <= finish time")
        return False
    return True 


def parse_from_cli():
    parser = argparse.ArgumentParser(description="Python script to read a workload trace in the Standard Workload Format (SWF), decompose it into user sessions, analyse the dependencies between sessions and store the results in the Session Annotated Batsim JSON format (SABjson).")
    parser.add_argument('input_swf',
                        type=argparse.FileType('r'),
                        help='The input SWF file')
    parser.add_argument('output_dir',
                        type=str,
                        help='The folder that will store the output files')

    delim_options = parser.add_mutually_exclusive_group(required=True)
    delim_options.add_argument(
        '-a',
        '--arrival',
        metavar="THRESHOLD",
        type=float,
        help="'Arrival' delimitation approach: a job starts a new "
        "session if the inter-arrival time with the last job "
        "is above the threshold (in minutes)")
    delim_options.add_argument(
        "-l",
        "--last",
        metavar="THRESHOLD",
        type=float,
        help="'Last' delimitation approach: a job starts a new "
        "session if the think time after the last job is above "
        "the threshold (in minutes)")
    delim_options.add_argument(
        "-m",
        "--max",
        metavar="THRESHOLD",
        type=float,
        help="'Max' delimitation approach: a job starts a new "
        "session if the think time after the previous job with "
        "the highest finish time is above the threshold (in minutes)")

    parser.add_argument(
        '--no_dynamic_reduction',
        action="store_true",
        help="Unless this option is specified, during the construction of the "
        "graph the algorithm dynamically avoids to add an edge between two "
        "nodes if a path already exists.")
    parser.add_argument(
        '--graph',
        action="store_true",
        help="Build a graphical representation of each session graph and save "
        "them in a subfolder as gml files")
    parser.add_argument(
        '--session_stat', action="store_true",
        help="Output a separate file `session_stat.json` containing summary "
        "information about the user sessions. In particular, it contains info "
        "about the original session durations (finish times) that are NOT "
        "embedded in the SABjsons."
    )
    parser.add_argument(
        '--no_SABjson_output', action="store_true",
        help="Disable SABjson output (for graph-only or session_stat-only usages)."
    )

    parser.add_argument("-q",
                        "--quiet",
                        action="store_true",
                        help="Lowest verbosity level.")

    job_options = parser.add_argument_group("job output options")
    job_options.add_argument(
        '-jwf',
        '--job_walltime_factor',
        type=float,
        default=2,
        help="Jobs walltimes are computed by the formula "
        "max(givenWalltime, jobWalltimeFactor*givenRuntime)")
    job_options.add_argument(
        '-gwo',
        '--given_walltime_only',
        action="store_true",
        help="If set, only the given walltime in the trace "
        "will be used")
    job_options.add_argument(
        '-jg',
        '--job_grain',
        type=int,
        default=1,
        help="(default: 1) Selects the level of detail we want for job profiles. "
        "This parameter is used to group jobs that have close running times. "
        "For example: a job grain of 10 will round up running times to the next ten.")
    

    args = parser.parse_args()

    if args.last is not None:
        delim = 'last'
        threshold = args.last
    elif args.max is not None:
        delim = 'max'
        threshold = args.max
    elif args.arrival is not None:
        delim = 'arrival'
        threshold = args.arrival
    else:  # should never happen
        raise argparse.ArgumentError(
            "You should specify a delimitation approach.")

    if threshold < 0:
        raise argparse.ArgumentTypeError(
            "The threshold must be a positive value.")

    if args.quiet:
        warnings.filterwarnings("ignore")

    swf2sessions(input_swf=args.input_swf,
                 out_dir=args.output_dir,
                 delim_approach=delim,
                 delim_threshold=threshold,
                 dynamic_reduction=not (args.no_dynamic_reduction),
                 build_graph_rep=args.graph,
                 session_stat=args.session_stat,
                 no_SABjson_output=args.no_SABjson_output,
                 job_walltime_factor=args.job_walltime_factor,
                 given_walltime_only=args.given_walltime_only,
                 job_grain=args.job_grain,
                 quiet=args.quiet)


if __name__ == "__main__":
    parse_from_cli()