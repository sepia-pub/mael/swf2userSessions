"""
The main building block of the program, where the cutting decisions are taken
according to the delimitation approach.
"""

from datetime import datetime
import networkx as nx
from swf2userSessions.workload import Session
import sys

class User:
    """Class representing a user of the workload."""

    def __init__(self,
                 user_id,
                 delim_approach,
                 delim_threshold,
                 dynamic_reduction=True,
                 build_graph_rep=False):
        self.id = user_id
        self.sessions = {}  # dictionnary of session_id, Session object

        self.__active_session_id = 0
        self.__last_submit_time = 0
        self.__last_finish_time = 0
        self.__max_finish_time = 0

        self.delim_approach = delim_approach
        self.delim_threshold = delim_threshold * 60  # minutes to sec

        self.dynamic_reduction = dynamic_reduction
        if dynamic_reduction:
            # set of sessions having unfinished jobs, at time t
            self.__sessions_in_progress = set()

            # finished sessions, constituting the minimal list of dependencies for a session starting at time t
            self.__current_dep = set()

        self.build_graph_rep = build_graph_rep
        if build_graph_rep:
            self.G = nx.DiGraph()  # the (directed) dependancy graph

        self.max_nb_res = 0

    ######## Private methods ########
    def __is_new_session(self, date_now):
        """Checks if a new session should be started according to the delimitation approach (see Zackay and Feitelson 2013)"""
        
        if len(self.sessions) == 0:
            return True

        if self.delim_approach == 'arrival':
            inter_arrival_time = date_now - self.__last_submit_time
            return (inter_arrival_time >= self.delim_threshold)

        elif self.delim_approach == 'last':
            think_time = date_now - self.__last_finish_time
            return (think_time >= self.delim_threshold)

        else:  # 'max' delimitation approach
            think_time = date_now - self.__max_finish_time
            return (think_time >= self.delim_threshold)

    def __sessions_finished_at_time(self, t):
        """Return the list of session that were finished at time t"""
        return [s for s in self.sessions.values() if s.id != self.__active_session_id and s.max_finish_time <= t]

    def __update_session_graph(self, date_now):
        """Add the dependencies for the active session"""
        active_session = self.sessions[self.__active_session_id]

        if self.build_graph_rep:
            self.G.add_node(self.__active_session_id)

        if not self.dynamic_reduction:
            # For all sessions that were finished when the current session was
            # started, add a directed edge weighted by the thinking time
            for finish_sess in self.__sessions_finished_at_time(date_now):
                think_time = date_now - finish_sess.max_finish_time  # >=0

                active_session.preceding_sessions.append(finish_sess.id)
                active_session.tt_after_prec_sess.append(think_time)

                if self.build_graph_rep:
                    self.G.add_edge(self.__active_session_id,
                                    finish_sess.id,
                                    weight=think_time)

        else:
            # Move the sessions recently finished from the set `_in_progress`
            # to the set `current_dep`, removing all their neighbours in the
            # dependancy graph from the set `current_dep` to keep it minimal
            for s_id in self.__sessions_in_progress.copy():
                sess = self.sessions[s_id]

                if date_now >= sess.max_finish_time:  # ie sess is finished
                    self.__sessions_in_progress.discard(s_id)
                    self.__current_dep = self.__current_dep - set(
                        sess.preceding_sessions)
                    self.__current_dep.add(s_id)

            # Add dependencies to active session
            for s_id in self.__current_dep:
                sess = self.sessions[s_id]
                think_time = date_now - sess.max_finish_time

                active_session.preceding_sessions.append(sess.id)
                active_session.tt_after_prec_sess.append(think_time)

                if self.build_graph_rep:
                    self.G.add_edge(self.__active_session_id,
                                    sess.id,
                                    weight=think_time)

    def __create_new_session(self, date_now):
        """Create a new active session and achive the old one"""

        # Archive active session and increment active_session_id
        if self.dynamic_reduction and self.__active_session_id > 0:
            self.__sessions_in_progress.add(self.__active_session_id)
        self.__active_session_id += 1

        # Create new session
        new_session = Session(self.__active_session_id, date_now)
        self.sessions[self.__active_session_id] = new_session

    def __add_job_to_active_session(self, job):
        active_sess = self.sessions[self.__active_session_id]
        active_sess.add_job(job)

    ######## Public methods ########
    def add_job(self, job):
        """Add a job to the job list of the user, creating a new session and 
        handling its dependencies, if needed."""

        if job.res > self.max_nb_res:
            self.max_nb_res = job.res
        date_now = job.submit_time

        # does the job start a new session?
        if self.__is_new_session(date_now):
            self.__create_new_session(date_now)
            self.__update_session_graph(date_now)
        self.__add_job_to_active_session(job)

        # Updating variables
        self.__last_submit_time = job.submit_time
        self.__last_finish_time = job.finish_time
        self.__max_finish_time = max(self.__max_finish_time, job.finish_time)

    def export_dependancy_graph(self, out_dir):
        """Write the dependancy graph as a gml file"""

        nx.write_gml(self.G, f"{out_dir}/{self.id}.gml")


    def to_dict(self, descr=None, cmd=None, version=1):
        """User object to dictionnary, for printing or SABjson export."""

        if descr is None:
            descr = f"Session Annotated Batsim JSON for user{self.id}"
        res = {
            "description": descr,
            "date": str(datetime.now()),
            "command": ' '.join(sys.argv[:]),
            "nb_res": self.max_nb_res,
            "version": version,
            "sessions": [s.to_dict() for _, s in self.sessions.items()]
        }
        if cmd is not None:
            res["command"] = cmd

        return res


    def to_session_stat(self):
        """Return a dictionnary with session stats"""

        return {
            "first_submit_time": min([s.first_submit for _, s in self.sessions.items()]),
            "max_finish_time": self.__max_finish_time,
            "last_submit_time": self.__last_submit_time,
            "last_finish_time": self.__last_finish_time,
            "nb_sessions": len(self.sessions),
            "sessions": [s.to_session_stat() for _, s in self.sessions.items()]
        }